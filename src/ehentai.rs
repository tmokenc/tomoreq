use crate::Result;
use std::fmt;

const API_ENDPOINT: &str = "https://api.e-hentai.org/api.php";

#[derive(Debug, Serialize, Deserialize)]
pub struct GmetadataRoot {
    pub gmetadata: Vec<Gmetadata>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GtokenRoot {
    pub tokenlist: Vec<Gtoken>,
}

#[derive(Default, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Gmetadata {
    pub gid: u64,
    pub token: String,
    pub archiver_key: String,
    pub title: Option<String>,
    pub title_jpn: Option<String>,
    pub category: Category,
    pub thumb: String,
    pub uploader: String,
    pub posted: String,
    pub filecount: String,
    pub filesize: u64,
    pub expunged: bool,
    pub rating: String,
    pub torrentcount: String,
    pub tags: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub enum Category {
    Doujinshi,
    Manga,
    #[serde(rename = "Artist CG")]
    ArtistCG,
    #[serde(rename = "Game CG")]
    GameCG,
    Western,
    #[serde(rename = "Image Set")]
    ImageSet,
    #[serde(rename = "Non-H")]
    NonH,
    Cosplay,
    AsianPorn,
    Misc,
}

impl Default for Category {
    fn default() -> Self {
        Self::Doujinshi
    }
}

impl fmt::Display for Category {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Category::*;

        match self {
            Doujinshi => write!(f, "Doujinshi"),
            Manga => write!(f, "Manga"),
            ArtistCG => write!(f, "Artist CG"),
            GameCG => write!(f, "Game CG"),
            Western => write!(f, "Western"),
            ImageSet => write!(f, "Image Set"),
            NonH => write!(f, "Non-H"),
            Cosplay => write!(f, "Cosplay"),
            AsianPorn => write!(f, "Asian Porn"),
            Misc => write!(f, "Misc"),
        }
    }
}

impl Category {
    /// Based on the color of the tag on e-h
    pub fn color(&self) -> u32 {
        use Category::*;

        match self {
            Doujinshi => 0xf66258,
            Manga => 0xf5a718,
            ArtistCG => 0xd4d503,
            GameCG => 0x09b60e,
            Western => 0x2cdb2b,
            ImageSet => 0x4f5ce7,
            NonH => 0x0cbacf,
            Cosplay => 0x902ede,
            AsianPorn => 0xf188ef,
            Misc => 0x878787,
        }
    }
}

#[cfg(feature = "extra")]
type TagList<'a> = Option<Vec<Tag<'a>>>;

#[cfg(feature = "extra")]
#[derive(Default)]
pub struct Tag<'a>(pub &'a str);

#[cfg(feature = "extra")]
impl fmt::Display for Tag<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[cfg(feature = "extra")]
impl Tag<'_> {
    pub fn wiki_url(&self) -> String {
        format!("https://ehwiki.org/wiki/{}", self.0.replace(' ', "_"))
    }
}

#[cfg(feature = "extra")]
#[derive(Default)]
pub struct Tags<'a> {
    pub artist: TagList<'a>,
    pub characters: TagList<'a>,
    pub group: TagList<'a>,
    pub parody: TagList<'a>,
    pub language: TagList<'a>,
    pub male: TagList<'a>,
    pub female: TagList<'a>,
    pub misc: TagList<'a>,
    pub reclass: Option<Tag<'a>>,
}

impl Gmetadata {
    pub fn is_sfw(&self) -> bool {
        self.category == Category::NonH || self.tags.contains(&"non-nude".into())
    }

    pub fn url(&self) -> String {
        format!("https://e-hentai.org/g/{}/{}", self.gid, self.token)
    }

    pub fn x_url(&self) -> String {
        format!("https://exhentai.org/g/{}/{}", self.gid, self.token)
    }

    #[cfg(feature = "extra")]
    pub fn parse_tags<'a>(&'a self) -> Tags<'a> {
        use magic::traits::MagicOption;

        let mut tags = Tags::default();

        for tag in self.tags.iter() {
            if tag.contains(':') {
                let mut iter = tag.split(':');
                let namespace = iter.next().unwrap();
                let value = Tag(iter.next().unwrap());

                match namespace {
                    "artist" => tags.artist.extend_inner(value),
                    "character" => tags.characters.extend_inner(value),
                    "group" => tags.group.extend_inner(value),
                    "language" => tags.language.extend_inner(value),
                    "male" => tags.male.extend_inner(value),
                    "female" => tags.female.extend_inner(value),
                    "parody" => tags.parody.extend_inner(value),
                    "reclass" => {
                        if tags.reclass.is_none() {
                            tags.reclass = Some(value)
                        }
                    }
                    _ => (),
                }
            } else {
                tags.misc.extend_inner(Tag(tag));
            }
        }

        tags
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Gtoken {
    pub gid: u64,
    pub token: String,
}

#[async_trait]
pub trait EhentaiApi: crate::Requester {
    async fn gmetadata<'a, I>(&self, g: I) -> Result<Vec<Gmetadata>>
    where
        I: IntoIterator<Item = (u32, &'a str)> + Send + 'async_trait,
    {
        #[derive(Serialize)]
        struct GmetadataBody<'a> {
            method: &'static str,
            gidlist: Vec<(u32, &'a str)>,
            namespace: u8,
        }

        let galleries = g.into_iter().collect::<Vec<_>>();
        let body = GmetadataBody {
            method: "gdata",
            gidlist: galleries,
            namespace: 1,
        };

        let data: GmetadataRoot = self.post(API_ENDPOINT, &body).await?;

        #[allow(unused_mut)]
        let mut gdata = data.gmetadata;

        #[cfg(feature = "extra")]
        {
            use escaper::decode_html;

            for mut d in gdata.iter_mut() {
                d.title = d
                    .title
                    .as_ref()
                    .filter(|v| !v.is_empty())
                    .and_then(|v| decode_html(v).ok());

                d.title_jpn = d
                    .title_jpn
                    .as_ref()
                    .filter(|v| !v.is_empty())
                    .and_then(|v| decode_html(v).ok());
            }
        }

        Ok(gdata)
    }

    async fn gtoken<T, P>(&self, id: u64, token: T, page: P) -> Result<Vec<Gtoken>>
    where
        T: AsRef<str> + Send + 'async_trait,
        P: IntoIterator<Item = u16> + Send + 'async_trait,
    {
        #[derive(Serialize)]
        struct GtokenBody<'a> {
            method: &'static str,
            pagelist: Vec<(u64, &'a str, u16)>,
        }

        let pages = page
            .into_iter()
            .map(|v| (id, token.as_ref(), v))
            .collect::<Vec<_>>();

        let body = GtokenBody {
            method: "gtoken",
            pagelist: pages,
        };

        let data: GtokenRoot = self.post(API_ENDPOINT, &body).await?;
        Ok(data.tokenlist)
    }
}

impl<R: crate::Requester> EhentaiApi for R {}
