use crate::{Requester, Result, Scraper};

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
struct DuckDuckGoImageJson {
    query: String,
    // queryEncoded: String,
    response_type: String,
    next: String,
    results: Vec<DuckDuckGoImageData>,
}

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
pub struct DuckDuckGoImageData {
    pub url: String,
    pub source: String,
    pub image: String,
    pub thumbnail: String,
    pub title: String,
    pub width: usize,
    pub height: usize,
}

#[derive(Debug, Clone, Copy)]
pub enum Filter {
    Nsfw,
    Sfw,
}

impl From<bool> for Filter {
    fn from(b: bool) -> Self {
        if b {
            Self::Sfw
        } else {
            Self::Nsfw
        }
    }
}

#[async_trait]
pub trait DuckDuckGoScraper: Requester + Scraper {
    /// # This is an undocumented API!!!
    /// ## It can be change without any warning!!!
    async fn duck_image<F: Into<Filter> + Sync + Send>(
        &self,
        text: &str,
        offset: usize,
        filter: F,
    ) -> Result<Vec<DuckDuckGoImageData>> {
        let vpd = self.get_vdp(&text).await?;
        self.duck_image_with_vdp(text, offset, filter, &vpd).await
    }

    async fn duck_image_with_vdp<F: Into<Filter> + Sync + Send>(
        &self,
        text: &str,
        offset: usize,
        filter: F,
        vpd: &str,
    ) -> Result<Vec<DuckDuckGoImageData>> {
        let sfw = matches!(filter.into(), Filter::Sfw);
        let url = format!(
            "https://duckduckgo.com/i.js?&o=json&q={}&vpd={}&ex={}&s={}",
            text,
            vpd,
            if sfw { 1 } else { -1 },
            offset,
        );

        let data: DuckDuckGoImageJson = self.get(&url).await?;

        Ok(data.results)
    }

    async fn get_vdp(&self, text: &str) -> Result<String> {
        let url = format!(
            "https://duckduckgo.com/?q={}&t=ha&iax=images&ia=images",
            text
        );
        let text = self.text(&url).await?;

        let result = (move || {
            let start = text.find("vqd='")? + 4;
            let end = text[start..].find('\'')? + start;

            Some(text[start..end].to_owned())
        })();

        result.ok_or_else(|| Box::new(magic::Error) as Box<_>)
    }
}

impl<S: Requester + Scraper> DuckDuckGoScraper for S {}
