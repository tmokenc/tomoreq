use crate::Result;

type UrbanResult = Result<Vec<UrbanDictionary>>;

#[derive(Deserialize)]
struct UrbanRoot {
    list: Vec<UrbanDictionary>,
}

#[derive(Deserialize)]
pub struct UrbanDictionary {
    pub word: String,
    pub definition: String,
    pub permalink: String,
    pub author: String,
    pub example: String,
    pub thumbs_up: u32,
    pub thumbs_down: u32,
    pub written_on: String,
}

#[async_trait]
pub trait UrbanApi: crate::Requester {
    async fn search_word(&self, word: &str) -> UrbanResult {
        let url = format!("http://api.urbandictionary.com/v0/define?term={}", word);
        self.get::<UrbanRoot>(&url).await.map(|v| v.list)
    }

    async fn get_random(&self) -> UrbanResult {
        let url = "http://api.urbandictionary.com/v0/random";
        self.get::<UrbanRoot>(&url).await.map(|v| v.list)
    }
}

impl<R: crate::Requester + ?Sized> UrbanApi for R {}
