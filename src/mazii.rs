use crate::Result;
use std::collections::HashMap;

const API_END_POINT: &str = "http://mazii.net/api/search";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MaziiKanjiSearch {
    pub status: u16,
    pub results: Vec<MaziiKanji>,
}

// Type Kanji
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MaziiKanji {
    pub kanji: char,
    pub mean: String,
    pub on: String,
    pub kun: Option<String>,
    pub detail: Option<String>,
    pub comp: Option<String>,
    pub level: Option<char>,
    pub stoke_count: Option<char>,
    pub example_on: Option<HashMap<String, Vec<Example>>>,
    pub example_kun: Option<HashMap<String, Vec<Example>>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Example {
    #[serde(alias = "w")]
    pub word: String,
    #[serde(alias = "p")]
    pub phonetic: String,
    #[serde(alias = "m")]
    pub meaning: String,
}

impl MaziiKanji {
    pub fn normal_detail(&self) -> Option<String> {
        self.detail.as_ref().map(|e| {
            e.split("##")
                .map(|v| format!("- {}\n", v))
                .collect::<String>()
        })
    }

    pub fn normal_on(&self) -> String {
        self.on.split(' ').collect::<Vec<&str>>().join("、")
    }

    pub fn normal_kun(&self) -> Option<String> {
        self.kun
            .as_ref()
            .map(|e| e.split(' ').collect::<Vec<_>>().join("、"))
    }
}

#[derive(Serialize)]
struct MaziKanjiBody<'a> {
    dict: &'a str,
    r#type: &'a str,
    query: &'a str,
    page: u16,
}

#[async_trait]
pub trait MaziiApi: crate::Requester {
    async fn kanji(&self, kanji: &str) -> Result<Vec<MaziiKanji>> {
        let body = MaziKanjiBody {
            dict: "javi",
            r#type: "kanji",
            query: kanji,
            page: 1,
        };

        let data: MaziiKanjiSearch = self.post(API_END_POINT, &body).await?;
        let mut result = data.results;
        result.sort_by_key(|a| kanji.chars().position(|x| x == a.kanji));

        Ok(result)
    }
}

impl<R: crate::Requester + ?Sized> MaziiApi for R {}
